#!/bin/bash

#
# Copyright (c) 2022, Łukasz Lasota
#

### Scripts env
export DWORK=/upload2/biala/infra/migCPD
export DBIN=$DWORK/bin
export DETC=$DWORK/etc
export DTMP=$DWORK/tmp
export DSQL=$DWORK/sql
export DLOG=$DWORK/log

### Oracle env
#unset ORACLE_HOME PATH
#. $DETC/oracle_rdbms.env

# instance name
export ORACLE_SID=$1

# database name
export DB_SID=${ORACLE_SID}

# dump file
export FDMP=${ORACLE_SID}
export LOGF=${DLOG}/${ORACLE_SID}.log
export SQLPLS="sqlplus / as sysdba"

#variables
export WERSJA_DIR="11.2.0.1se"
export WERSJA="11.2.0.1"
export ORACLE_HOME="/opt/oracle/product/11.2.0/dbhome_1"
export ORACLE_HOME_NEW="/orabin/${WERSJA_DIR}/product/dbhome_1"
export ORACLE_DBOLD="/opt/oracle/oradata11/${ORACLE_SID}"
export ORACLE_DBDATA="/oradata/${WERSJA_DIR}/${ORACLE_SID}"
export NEW_WORK_DIR="/oradata/DUMP/${ORACLE_SID}"
export BCK="/opt/oracle/DUMP"

delete_database_rac()
{
    _log_msg DeleteDB START
    #bash $DBIN/deleteBD.sh ${DB_SID}
    _log_msg DeleteDB STOP

    #rm -r /ora02/oradata/SZP
}

create_dir_datapump_old()
{
$SQLPLS <<EOF 
    create or replace directory BCK as '${BCK}';
    grant read,write on directory BCK to system;
EOF
}

stop_database()
{
    _log_msg StopDB ${DB_SID}
    $SQLPLS <<< "shutdown immediate;"
}

start_database()
{
    _log_msg StartDB ${DB_SID}
    $SQLPLS <<< "startup;"
}

getdb-ddl_tablespaces()
{
sqlplus -S / as sysdba <<!

spool ${DSQL}/${DB_SID}_create_tablespace.sql;
@${DETC}/checkdb/getdb-ddl_tablespaces.sql ${WERSJA_DIR} ${ORACLE_SID};
spool off;
!
}

exp_full()
{
    $SQLPLS <<< "alter user system identified by system;"
    $SQLPLS <<< "truncate table sys.aud$;"
    $SQLPLS <<< "alter system set undo_retention=20000 scope=both;"
    expdp system/system full=Y directory=BCK dumpfile=${FDMP}.dmp logfile=${ORACLE_SID}.log parfile=${DETC}/parfile.par
    $SQLPLS <<< "alter system set undo_retention=900 scope=both;"
}

cpy_tablespace_def()
{
    scp ${DSQL}/${DB_SID}_create_tablespace.sql oracle@pozsrvdb1:${NEW_WORK_DIR}
}

cpy_dumpfile()
{
    scp ${BCK}/${ORACLE_SID}.dmp oracle@pozsrvdb1:${NEW_WORK_DIR}
}

new_server()
{
ssh oracle@pozsrvdb1 <<EOF
    1
export system_pass=system
export NEW_WORK_DIR=${NEW_WORK_DIR}
export WERSJA_DIR=$WERSJA_DIR
export ORACLE_SID=$ORACLE_SID
    /oradata/DUMP/createDB11.sh
    mkdir ${NEW_WORK_DIR}
    exit
EOF
}

old_server()
{
    getdb-ddl_tablespaces
    ##mkdir ${BCK}
    create_dir_datapump_old
    cpy_tablespace_def
    exp_full
    cpy_dumpfile
}

new_server_after_exp()
{
ssh oracle@pozsrvdb1 <<EOF
    1
    export NEW_WORK_DIR=${NEW_WORK_DIR}
    export DB_SID=${ORACLE_SID}
    export ORACLE_SID=${ORACLE_SID}
    #create_tablespaces
    $SQLPLS @${NEW_WORK_DIR}/${DB_SID}_create_tablespace.sql; &>> ${NEW_WORK_DIR}/${DB_SID}_create_tablespace.log
    #create_dir_datapump_new
    $SQLPLS <<< "create or replace directory BCK as '${NEW_WORK_DIR}';"
    $SQLPLS <<< "grant read,write on directory BCK to system;"
    #imp_full_rac
    impdp system/system directory=BCK dumpfile=${FDMP}.dmp logfile=imp-${ORACLE_SID}.log
    $SQLPLS <<< "@?/rdbms/admin/utlrp"
    #gather_stats
    $SQLPLS <<< "exec dbms_stats.gather_database_stats(cascade=> TRUE, estimate_percent=> 20, method_opt=> 'FOR ALL COLUMNS SIZE AUTO');"
    exit
EOF
}

##################################
                   ##   BEGIN   ##
                   ###############
new_server
old_server
new_server_after_exp


echo "#################" >> $LOGF
                    ###############
                    ##    END    ##
###################################

