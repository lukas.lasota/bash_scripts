#!/usr/bin/bash

export WERSJA="11.2.0.1"
export ORACLE_HOME="/opt/oracle/product/11.2.0/dbhome_1"
export ORACLE_HOME_NEW="/orabin/11.2.0.1/product/dbhome_1"
export WORK_DIR="/upload2/biala/infra/migCPD"
export ORACLE_SID=$1
#export ORACLE_SID=$OLD_SID
export ORACLE_DBOLD="/opt/oracle/oradata11/${ORACLE_SID}"
export ORACLE_DBDATA="/oradata/11.2.0.1/${ORACLE_SID}"
export CLORA_LOG_DIR="/var/log/oracle"
export CLORA_LOG="${CLORA_LOG_DIR}/${ORACLE_SID}"

#Zmienne dla wyciągnięcia trace oraz obróbki ich
tmp="/upload2/biala/infra/migCPD/bin/tmp"
src_script="/upload2/biala/infra/migCPD/migTrace/11.2.0.1/${ORACLE_SID}.trace"
dst_script="/upload2/biala/infra/migCPD/migTrace/11.2.0.1/${ORACLE_SID}_NEW.trace"
pfile="/upload2/biala/infra/migCPD/migTrace/11.2.0.1/${ORACLE_SID}.pfile"
kp1_data="/opt/oracle/oradata11/${ORACLE_SID}/"

drop_old_directory()
{
echo "Tworzenie katalogu dla bazy"
    rm -r ${ORACLE_DBDATA}

echo "Tworzenie katalogu admin"

    rm -r ${ORACLE_DBDATA}/admin

    rm -r ${ORACLE_DBDATA}/admin/adump

    rm -r ${ORACLE_DBDATA}/admin/bdump

    rm -r ${ORACLE_DBDATA}/admin/cdump

    rm -r ${ORACLE_DBDATA}/admin/ddump

    rm -r ${ORACLE_DBDATA}/admin/udump
}


create_directory()
{
echo "Tworzenie katalogu dla bazy"
    mkdir -p ${ORACLE_DBDATA}

echo "Tworzenie katalogu admin"

    mkdir -p ${ORACLE_DBDATA}/admin

    mkdir -p ${ORACLE_DBDATA}/admin/adump

    mkdir -p ${ORACLE_DBDATA}/admin/bdump

    mkdir -p ${ORACLE_DBDATA}/admin/cdump

    mkdir -p ${ORACLE_DBDATA}/admin/ddump

    mkdir -p ${ORACLE_DBDATA}/admin/udump

    #mkdir -p ${ORACLE_HOME}/cfgtoollogs/dbca/$ORACLE_SID

    #mkdir -p ${ORACLE_DBDATA}/flash_recovery_area

}

get_trace()
{
    sqlplus -S / as sysdba <<< "alter database backup controlfile to trace as '/upload2/biala/infra/migCPD/migTrace/11.2.0.1/\${ORACLE_SID}.trace';"
}

edit_trace()
{
# $1 - src script
# $2 - dst script
# $3 - nazwa instancji

#export ORACLE_SID=admmd28

echo
echo "Zapisuje zmieniony plik $1 jako $2:"
cat > $2 <<EOF
CREATE CONTROLFILE REUSE SET DATABASE "$3" RESETLOGS NOARCHIVELOG
    MAXLOGFILES 6
    MAXLOGMEMBERS 3
    MAXDATAFILES 1024
    MAXINSTANCES 1
    MAXLOGHISTORY 16562
EOF

sed -n '/Set #2/,/;/p' $1 | sed -n '/^LOGFILE/,$p' | sed '/-- STANDBY LOGFILE/d;/^$/d' >> $2

cat >> $2 <<EOF

ALTER DATABASE RECOVER USING BACKUP CONTROLFILE UNTIL CANCEL;

ALTER DATABASE RECOVER CANCEL;

ALTER DATABASE OPEN RESETLOGS;

EOF
sed -n '/Set #2/,$p' $1 | sed -n '/^ALTER TABLESPACE/,/#/p' | grep -v End >> $2

if [ "$3" == "kp1" ]; then
cat $2 | sed -i "s|/opt/oracle/oradata11/${ORACLE_SID}|/oradata/11.2.0.1/${ORACLE_SID}/${ORACLE_SID}|g" > $tmp/dst_$3.sql
mv $tmp/dst_$3.sql $2
fi

sed -i "s|/opt/oracle/oradata11/${ORACLE_SID}|/oradata/11.2.0.1/${ORACLE_SID}/${ORACLE_SID}|g" $dst_script
sed -i '/^ *$/d' $dst_script

#$chown $dbuser $2
#$chmod u+x $2
echo "[Done]"
echo $ORACLE_SID
}

get_init()
{
    sqlplus -S / as sysdba <<< "create pfile='/upload2/biala/infra/migCPD/migTrace/11.2.0.1/$ORACLE_SID.pfile' from spfile;"
    sed -i "s|/opt/oracle/oradata11/${ORACLE_SID}|/oradata/11.2.0.1/${ORACLE_SID}|g" $pfile
    sed -i "s|/opt/oracle/admin/${ORACLE_SID}|/oradata/11.2.0.1/${ORACLE_SID}/admin|g" $pfile
    sed -i "s|/opt/oracle|/orabin/11.2.0.1|g" $pfile
}

db_stop()
{
echo "Zamykam instancje $ORACLE_SID:"
    export ORACLE_SID=$ORACLE_SID
    sqlplus / as sysdba <<< "shutdown immediate;"
}

db_startup_nomount()
{
echo "Startuje instancje $ORACLE_SID w trybie nomount:"
    export ORACLE_HOME=$ORACLE_HOME_NEW 
    export ORACLE_SID=$ORACLE_SID
    sqlplus / as sysdba <<< "startup nomount;"
}

db_startup()
{
echo "Startuje instancje $ORACLE_SID:"
    export ORACLE_SID=$ORACLE_SID
    sqlplus / as sysdba <<< "startup;"
}

run_current_db()
{
    echo "Startuje instancje $ORACLE_SID:"
    export ORACLE_SID=$ORACLE_SID
    sqlplus / as sysdba <<< "startup;"
}

copy_db()
{
    scp -r /opt/oracle/oradata11/${ORACLE_SID} oracle@pozsrvdb1:$ORACLE_DBDATA
    export ORACLE_HOME=$ORACLE_HOME
    cd $ORACLE_HOME/dbs
    find . -name "orapw${ORACLE_SID}*" -exec scp {} oracle@pozsrvdb1:${ORACLE_HOME_NEW}/dbs \;

    scp $pfile oracle@pozsrvdb1:${ORACLE_HOME_NEW}/dbs
    scp $dst_script oracle@pozsrvdb1:${ORACLE_DBDATA}
}

db_run_script()
{
    export ORACLE_HOME=$ORACLE_HOME_NEW
    export ORACLE_SID=$ORACLE_SID
    sqlplus / as sysdba @${ORACLE_DBDATA}/${ORACLE_SID}_NEW.trace
}

old_server()
{
    get_trace
    edit_trace $src_script $dst_script $ORACLE_SID
    get_init
    db_stop
    sleep 3
    copy_db
    db_startup
}

new_server()
{
#ssh -i ~/.ssh/id_dsa oracle@pozsrvdb1 "$(declare -f create_directory);create_directory"
    ##--drop_directory
#    create_directory
#EOF
#    logout

#druga wersja:
ssh oracle@pozsrvdb1 <<EOF
    export ORACLE_DBDATA="/oradata/11.2.0.1/$ORACLE_SID"
    
    rm -r ${ORACLE_DBDATA}
    rm ${ORACLE_HOME_NEW}/dbs/*${ORACLE_SID}*
    
    mkdir -p ${ORACLE_DBDATA}
    mkdir -p ${ORACLE_DBDATA}/admin
    mkdir -p ${ORACLE_DBDATA}/admin/adump
    mkdir -p ${ORACLE_DBDATA}/admin/bdump
    mkdir -p ${ORACLE_DBDATA}/admin/cdump
    mkdir -p ${ORACLE_DBDATA}/admin/ddump
    mkdir -p ${ORACLE_DBDATA}/admin/udump
    exit
EOF
}

new_server_after_copy()
{
ssh oracle@pozsrvdb1 <<EOF
    #db_startup_nomount
    echo "Startuje instancje $ORACLE_SID w trybie nomount:"
    export ORACLE_HOME=$ORACLE_HOME_NEW 
    export ORACLE_SID=$ORACLE_SID
    ${ORACLE_HOME_NEW}/bin/sqlplus / as sysdba <<< "shutdown immediate;"
    ${ORACLE_HOME_NEW}/bin/sqlplus / as sysdba <<< "startup nomount pfile='${ORACLE_HOME_NEW}/dbs/${ORACLE_SID}.pfile';"
            
    #db_run_script
    ${ORACLE_HOME_NEW}/bin/sqlplus / as sysdba @${ORACLE_DBDATA}/${ORACLE_SID}_NEW.trace
    
    create spfile='${ORACLE_HOME_NEW}/dbs/spfile${ORACLE_SID}.ora' FROM pfile='${ORACLE_HOME_NEW}/dbs/${ORACLE_SID}.pfile';
    shutdown immediate;
    startup;
    exit
EOF
}

############ Proces migracji

new_server
old_server
new_server_after_copy
#run_current_db

