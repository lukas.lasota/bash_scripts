#!/bin/bash
unset ORACLE_HOME PATH
export NLS_LANG=POLISH_POLAND.EE8MSWIN1250

if [[ -z $1 || -z $2 || -z $3 ]]; then
  echo "Nie podałeś wymaganych parametrów"
  echo "Podaj SID bazy, wersje Oracle oraz nazwe pliku dump (bez rozszerzenia .dmp)  Np: createDB.sh medab01 12201 szpitalZOZ"
  exit 1
  else
    if [[ "${#1}" < 7 || "${#1}" > 8 ]]; then
    echo "Błąd. SID bazy ma niepoprawną liczbę znaków (powinien mieć między 7 lub 8 znaków)"
    exit 1
    elif [[ $2 != '11201' && $2 != '11204' && $2 != '12102' && $2 != '12201' && $2 != '19300' ]]; then
    echo "Błąd. nie wybrano odpowiedniej wersji Oracle 112011 lub 11204 lub 12102 lub 12201 lub 19300"
    exit 1
    elif [[ ! -f "$3.dmp" ]]; then
    echo "Błąd. W tym katalogu nie ma pliku o nazwie: "$3".dmp"
    exit 1
    fi
fi
#Oracle environment
if [[ $2 = '12102' ]]; then
export ORACLE_BASE=/orabin/12.1.0.2se
else
export ORACLE_BASE=/orabin
fi

#export ORACLE_BASE=/orabin

if [[ $2 = '11201' ]]; then
export ORACLE_HOME=$ORACLE_BASE/11.2.0.1se/product/dbhome_1
export TEMPLATEVER='11.2.0.1se'
elif [[ $2 = '11204' ]]; then
export ORACLE_HOME=$ORACLE_BASE/11.2.0.4se/product/dbhome_1
export TEMPLATEVER='11.2.0.4se'
elif [[ $2 = '12102' ]]; then
export ORACLE_HOME=$ORACLE_BASE/product/dbhome_1
export TEMPLATEVER='12.1.0.2se'
elif [[ $2 = '12201' ]]; then
export ORACLE_HOME=$ORACLE_BASE/12.2.0.1se/product/dbhome_1
export TEMPLATEVER='12.2.0.1se'
elif [[ $2 = '19300' ]]; then
export ORACLE_HOME=$ORACLE_BASE/19.3.0.0se/product/dbhome_1
export TEMPLATEVER='19.3.0.0se'
else
export ORACLE_HOME=''
export TEMPLATEVER=''

fi

export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/lib:/usr/lib
CLASSPATH=$ORACLE_HOME/jlib:$ORACLE_HOME/rdbms/jlib; export CLASSPATH
export PATH=$ORACLE_HOME/bin:$ORACLE_HOME/OPatch:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/oracle/.local/bin:/home/oracle/bin

echo "ORACLE_HOME="$ORACLE_HOME

export ORACLE_SID=$1

export file_=$3
export DIRDMP=$(pwd)
echo "DIR_DMP="$DIRDMP
echo "START_TIME="$(date '+%d/%m/%Y %H:%M:%S')

export parfile_name=imp-parfile
export HOME=$(pwd)
#export DIRRSP=${HOME}/rsp
export DIRSQL=${HOME}/sql
#export DIRWORK=${HOME}/${ODB_}

export system_pass=system

_dbca_createdb()
{
dbca -silent -createDatabase -databaseType MULTIPURPOSE \
-templateName ${ORACLE_HOME}/assistants/dbca/templates/${TEMPLATEVER}.dbt \
-gdbName ${ORACLE_SID} \
-sid ${ORACLE_SID} \
-sysPassword $system_pass -systemPassword $system_pass \
-emConfiguration NONE \
-storageType FS \
-redoLogFileSize 200 \
-characterSet EE8MSWIN1250 -nationalCharacterSet AL16UTF16
}

_dbca_createdb12()
{
dbca -silent -createDatabase -databaseType MULTIPURPOSE \
-databaseConfigType SINGLE \
-createAsContainerDatabase false \
-templateName ${ORACLE_HOME}/assistants/dbca/templates/${TEMPLATEVER}.dbt \
-gdbName ${ORACLE_SID} \
-sid ${ORACLE_SID} \
-sysPassword $system_pass -systemPassword $system_pass \
-emConfiguration NONE \
-enableArchive false \
-storageType FS \
-datafileDestination /oradata/${TEMPLATEVER}/${ORACLE_SID} \
-redoLogFileSize 200 \
-characterSet EE8MSWIN1250 -nationalCharacterSet AL16UTF16
}


_cr_oracle_dir()
{
echo "#### _cr_oracle_dir"
echo "create or replace directory SERWIS as '$DIRDMP';"|sqlplus -S / as sysdba
echo "GRANT READ, WRITE ON DIRECTORY SERWIS TO system;"|sqlplus -S / as sysdba
}

_undo_before()
{
echo "#### _undo_before"
echo "alter system set undo_retention=36000 sid='*';"|sqlplus -S / as sysdba
}

_undo_after()
{
echo "#### _undo_after"
echo "alter system set undo_retention=900 sid='*';"|sqlplus -S / as sysdba
}

_alter_user_system()
{
echo "#### _alter_user_system"
echo "alter user system identified by $system_pass;"|sqlplus -S / as sysdba
}

_create_user_sys()
{
echo "#### _create_user_LBACSYS_and_MDSYS_and_ORDSYS"
echo "create user LBACSYS identified by LBACSYS;"|sqlplus -S / as sysdba
echo "create user MDSYS identified by MDSYS;"|sqlplus -S / as sysdba
echo "create user ORDSYS identified by ORDSYS;"|sqlplus -S / as sysdba
echo "grant unlimited tablespace to ORDSYS;"|sqlplus -S / as sysdba
echo "grant unlimited tablespace to LBACSYS;"|sqlplus -S / as sysdba
echo "grant unlimited tablespace to MDSYS;"|sqlplus -S / as sysdba
}


_imp_full()
{
echo "#### _imp_full"
impdp system/"$system_pass" dumpfile=${file_}.dmp logfile=${file_}-IMP.log parfile=$parfile_name
}

_stats_database()
{
echo "exec dbms_stats.gather_database_stats;"|sqlplus -S / as sysdba
}

_compile_all()
{
echo "@?/rdbms/admin/utlrp.sql;"|sqlplus -S / as sysdba
}

######
# GET
######
_get_ddl_tbls()
{
echo "#### _get_ddl_tbls"
impdp system/"$system_pass" dumpfile=${file_}.dmp logfile=${file_}.imp.ddl-tbls DIRECTORY=SERWIS sqlfile=${ORACLE_SID}-ddl-tbls.sql INCLUDE=TABLESPACE
cat ${DIRDMP}/${ORACLE_SID}-ddl-tbls.sql |grep "CREATE TABLESPACE"|sed -e "s/$/SIZE 100M AUTOEXTEND ON NEXT 300M MAXSIZE UNLIMITED;/g" > ${DIRDMP}/${ORACLE_SID}-tbls.sql
cat ${DIRDMP}/${ORACLE_SID}-ddl-tbls.sql |grep "CREATE BIGFILE"|sed -e "s/$/SIZE 1G  AUTOEXTEND ON NEXT 400M MAXSIZE UNLIMITED;/g" >> ${DIRDMP}/${ORACLE_SID}-tbls.sql
cat ${DIRDMP}/${ORACLE_SID}-ddl-tbls.sql |grep "CREATE TEMPORARY "|sed -e "s/$/SIZE 1G AUTOEXTEND ON NEXT 400M MAXSIZE UNLIMITED;/g" >> ${DIRDMP}/${ORACLE_SID}-tbls.sql

for x in   SZP_INDEKSY
do
 for i in {0..7}
 do
  echo "ALTER TABLESPACE $x ADD DATAFILE SIZE 1000K  AUTOEXTEND ON NEXT 300M MAXSIZE UNLIMITED;" >> ${DIRDMP}/${ORACLE_SID}-tbls.sql
 done
done
for x in  SZP_DODATK SZP_GLOWNY SZP_LECZENIE
do
 for i in {0..5}
 do
  echo "ALTER TABLESPACE $x ADD DATAFILE SIZE 1000K  AUTOEXTEND ON NEXT 300M MAXSIZE UNLIMITED;" >> ${DIRDMP}/${ORACLE_SID}-tbls.sql
 done
done
}

_get_ddl_grants()
{
echo "#### _get_ddl_grants"
impdp system/"$system_pass" dumpfile=${file_}.dmp logfile=${file_}.imp.ddl-grants DIRECTORY=SERWIS sqlfile=${ORACLE_SID}-ddl-grants.sql INCLUDE=GRANT
}

######
# PUT
######
_put_ddl_grants()
{
echo "#### _put_ddl_grants"
grep "^GRANT" $DIRDMP/${ORACLE_SID}-ddl-grants.sql|sqlplus / as sysdba
}

_put_ddl_tbls()
{
echo "#### _put_ddl_tbls"
cat ${DIRDMP}/${ORACLE_SID}-tbls.sql | sqlplus -S / as sysdba
}

_put_sys_privs()
{
echo "#### _put_sys_privs"
cat ${DIRDMP}/${ORACLE_SID}-sys_privs.sql|sqlplus -S / as sysdba
}

_put_dblinks()
{
echo "#### _put_dblinks"
sqlplus -S / as sysdba @${DIRDMP}/${ORACLE_SID}-dblinks.sql
}

# DISABLE
_disable_case_sensitive()
{
echo "#### _disable_case_sensitive"
echo "alter system set sec_case_sensitive_logon=false scope=both sid='*';"|sqlplus -S / as sysdba;
}

_disable_features_12c()
{
echo "#### _disable_features_12c"
cat ${DIRSQL}/disable_12c.sql| sqlplus -S / as sysdba
}


#BEGIN

if [[ $2 = '12201' || $2 = '19300' ]]; then
_dbca_createdb12
else
_dbca_createdb
fi

_cr_oracle_dir
_alter_user_system
_create_user_sys

_get_ddl_tbls
_put_ddl_tbls

_undo_before
_imp_full
_undo_after
_alter_user_system

#_get_ddl_grants
#_put_ddl_grants
##_put_sys_privs
##_put_dblinks
##_disable_case_sensitive
##_disable_features_12c

#_compile_all

#_stats_database
echo "STOP_TIME="$(date '+%d/%m/%Y %H:%M:%S')
exit 0;
#END

