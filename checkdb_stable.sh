#!/bin/bash
#
# Copyright (c) 2022, Łukasz Lasota
#

export ORACLE_HOME=`egrep -i ":Y|:N" /etc/oratab | cut -d":" -f2 | grep -v "\#" | grep -v "\*" | grep -i "12" | sort -u`
export sqlplus="sqlplus / as sysdba"

#DB=`egrep -i ":Y|:N" /etc/oratab | cut -d":" -f1 | grep -v "\#" | grep -v "\*"`

export DBALL=`ps -ef | grep pmon | grep -v grep | awk '{ print $8 }' | cut -d '_' -f3 | grep -v ASM`

for run in $DBALL; do
    export ORACLE_SID=$run
    $sqlplus @/home/oracle/test/checkdb_basic.sql &>> /home/oracle/test/log.log;
done

exit